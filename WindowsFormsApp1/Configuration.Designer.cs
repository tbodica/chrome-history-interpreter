﻿namespace WindowsFormsApp1
{
    partial class Configuration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1_ChromePath = new System.Windows.Forms.Label();
            this.button1BrowseChrome = new System.Windows.Forms.Button();
            this.textBox1_chromePath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox1_CopyDB = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1_ChromePath
            // 
            this.label1_ChromePath.AutoSize = true;
            this.label1_ChromePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1_ChromePath.Location = new System.Drawing.Point(23, 69);
            this.label1_ChromePath.Name = "label1_ChromePath";
            this.label1_ChromePath.Size = new System.Drawing.Size(119, 16);
            this.label1_ChromePath.TabIndex = 0;
            this.label1_ChromePath.Text = "Path to chromeDB:";
            // 
            // button1BrowseChrome
            // 
            this.button1BrowseChrome.Location = new System.Drawing.Point(561, 65);
            this.button1BrowseChrome.Name = "button1BrowseChrome";
            this.button1BrowseChrome.Size = new System.Drawing.Size(75, 23);
            this.button1BrowseChrome.TabIndex = 1;
            this.button1BrowseChrome.Text = "Browse";
            this.button1BrowseChrome.UseVisualStyleBackColor = true;
            this.button1BrowseChrome.Click += new System.EventHandler(this.Button1BrowseChrome_Click);
            // 
            // textBox1_chromePath
            // 
            this.textBox1_chromePath.Enabled = false;
            this.textBox1_chromePath.Location = new System.Drawing.Point(148, 68);
            this.textBox1_chromePath.Name = "textBox1_chromePath";
            this.textBox1_chromePath.Size = new System.Drawing.Size(407, 20);
            this.textBox1_chromePath.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(26, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // checkBox1_CopyDB
            // 
            this.checkBox1_CopyDB.AutoSize = true;
            this.checkBox1_CopyDB.Location = new System.Drawing.Point(26, 113);
            this.checkBox1_CopyDB.Name = "checkBox1_CopyDB";
            this.checkBox1_CopyDB.Size = new System.Drawing.Size(118, 17);
            this.checkBox1_CopyDB.TabIndex = 4;
            this.checkBox1_CopyDB.Text = "Work with DB copy";
            this.checkBox1_CopyDB.UseVisualStyleBackColor = true;
            // 
            // Configuration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 386);
            this.Controls.Add(this.checkBox1_CopyDB);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1_chromePath);
            this.Controls.Add(this.button1BrowseChrome);
            this.Controls.Add(this.label1_ChromePath);
            this.Name = "Configuration";
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.Configuration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1_ChromePath;
        private System.Windows.Forms.Button button1BrowseChrome;
        private System.Windows.Forms.TextBox textBox1_chromePath;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox1_CopyDB;
    }
}