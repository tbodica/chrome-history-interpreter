﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Text.RegularExpressions;
using WindowsFormsApp1.Models;

namespace WindowsFormsApp1
{
    public partial class SearchPdfForm : Form
    {
        IGenericDatabaseSearchController searchController;
        string searchText = "";

        public SearchPdfForm()
        {
            InitializeComponent();
        }

        private void PopulateRichTextBox()
        {
            this.richTextBox1Res.Clear();
            foreach (var result in searchController.results)
            {
                var resString = result.ToString();
                var currentType = result.GetResultType();

                var radioButtonsTypes = new Dictionary<string, RadioButton>
                {
                    { "All", this.radioButton1 },
                    { "search", this.radioButton4 }
                };

                var searchpattern = "";
                if (this.searchText == "")
                    searchpattern = ".*";
                else
                    searchpattern = this.searchText;

                try
                {
                    var regexRes = Regex.Match(resString, searchpattern);
                    if (!regexRes.Success)
                    {
                        continue;
                    }
                }
                catch { return; }

                // compare: corresponding radiobutton for result type is SET
                if (!radioButtonsTypes[result.GetResultType()].Checked)
                {
                    continue;
                }

                this.richTextBox1Res.AppendText(resString); // point of populating rtb1 with .results
            }
        }

        private void SearchPdfForm_Load(object sender, EventArgs e)
        {
            searchController = new IGenericDatabaseSearchController("pdf_urls");
            PopulateRichTextBox();
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButton1.Checked == false)
                return;
            PopulateRichTextBox();
        }

        private void RadioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButton4.Checked == false)
                return;
            PopulateRichTextBox();
        }

        private void TextBox1Search_TextChanged(object sender, EventArgs e)
        {
            this.searchText = textBox1Search.Text;
            PopulateRichTextBox();
        }
    }
}
