﻿namespace WindowsFormsApp1
{
    partial class SearchWikiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label2TotalCount = new System.Windows.Forms.Label();
            this.label1Counter = new System.Windows.Forms.Label();
            this.richTextBox1Res = new System.Windows.Forms.RichTextBox();
            this.textBox1Search = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(479, 169);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(70, 17);
            this.radioButton4.TabIndex = 23;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Searches";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.RadioButton4_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(479, 146);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(74, 17);
            this.radioButton1.TabIndex = 20;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "All Results";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButton1_CheckedChanged);
            // 
            // label2TotalCount
            // 
            this.label2TotalCount.AutoSize = true;
            this.label2TotalCount.Location = new System.Drawing.Point(60, 109);
            this.label2TotalCount.Name = "label2TotalCount";
            this.label2TotalCount.Size = new System.Drawing.Size(0, 13);
            this.label2TotalCount.TabIndex = 15;
            // 
            // label1Counter
            // 
            this.label1Counter.AutoSize = true;
            this.label1Counter.Location = new System.Drawing.Point(12, 72);
            this.label1Counter.Name = "label1Counter";
            this.label1Counter.Size = new System.Drawing.Size(41, 13);
            this.label1Counter.TabIndex = 14;
            this.label1Counter.Text = "Count: ";
            // 
            // richTextBox1Res
            // 
            this.richTextBox1Res.Location = new System.Drawing.Point(12, 88);
            this.richTextBox1Res.Name = "richTextBox1Res";
            this.richTextBox1Res.Size = new System.Drawing.Size(431, 350);
            this.richTextBox1Res.TabIndex = 13;
            this.richTextBox1Res.Text = "";
            // 
            // textBox1Search
            // 
            this.textBox1Search.Location = new System.Drawing.Point(12, 32);
            this.textBox1Search.Name = "textBox1Search";
            this.textBox1Search.Size = new System.Drawing.Size(344, 20);
            this.textBox1Search.TabIndex = 12;
            this.textBox1Search.TextChanged += new System.EventHandler(this.TextBox1Search_TextChanged);
            // 
            // SearchWikiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 450);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label2TotalCount);
            this.Controls.Add(this.label1Counter);
            this.Controls.Add(this.richTextBox1Res);
            this.Controls.Add(this.textBox1Search);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(647, 489);
            this.MinimumSize = new System.Drawing.Size(647, 489);
            this.Name = "SearchWikiForm";
            this.Text = "SearchWikiForm";
            this.Load += new System.EventHandler(this.SearchWikiForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label2TotalCount;
        private System.Windows.Forms.Label label1Counter;
        private System.Windows.Forms.RichTextBox richTextBox1Res;
        private System.Windows.Forms.TextBox textBox1Search;
    }
}