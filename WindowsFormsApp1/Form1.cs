﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microsoft.Data.Sqlite;
using System.Text.RegularExpressions;

using MySql.Data;
using MySql.Data.MySqlClient;

using Microsoft.Extensions.Configuration;
using WindowsFormsApp1.Models;
using MySqlX.XDevAPI.Relational;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public string AppPath { get; private set; }
        public string ResourcePath { get; private set; }

        public string HistoryPath { get; private set; }

        readonly MySqlConnection browsingConnection;

        private readonly IConfiguration SqlConfig;
        private readonly List<ConfiguredSite> configuredSites;

        public Form1()
        {
            InitializeComponent();

            AppPath = AppDomain.CurrentDomain.BaseDirectory;
            ResourcePath = AppPath + "\\Resource\\";
            richTextBoxCounter.AppendText("Statistics window initialized.\r\n");
            richTextBoxLog.AppendText("Log window initialized.\r\n");

            
            SqlConfig = new ConfigurationBuilder()
                .AddJsonFile("SqlConfig.json", optional: false, reloadOnChange: false)
                .Build();


            IConfiguration sitesConfig = new ConfigurationBuilder()
                .AddJsonFile("SitesConfig.json", optional: false, reloadOnChange: false)
                .Build();

            IConfiguration connString = new ConfigurationBuilder()
                .AddJsonFile("ConnectionString.json", optional: false, reloadOnChange: false)
                .Build();


            // todo += and parametrize db_name, then move main string in config
            var connectionString = String.Format("server = localhost; port = 3306; userid = {0}; database = browsing; password = {1}; CertificateFile=C:\\Users\\tbodi\\OneDrive\\Documents\\Code\\MariaDB\\SSL_Certs\\ca.pem;SslMode=Required;",
                SqlConfig.GetSection("Credentials").GetSection("username").Value,
                SqlConfig.GetSection("Credentials").GetSection("password").Value
                );

            // todo if (this.useConnectionStringExplicit == true)
            connectionString = connString.GetSection("ConnString").GetSection("connStr").Value;

            browsingConnection = new MySqlConnection(connectionString);
            configuredSites = new List<ConfiguredSite>();


            foreach (var element in sitesConfig.GetSection("Sites").GetChildren())
            {
                Int64 read_Id = Int64.Parse(element.GetSection("id").Value.ToString());
                string SiteName = element.GetSection("site_name").Value.ToString();

                var cUrl = element.GetSection("url").Value.ToString();
                bool useDb = bool.Parse(element.GetSection("use_db").Value.ToString());
                bool youtube = bool.Parse(element.GetSection("youtube").Value.ToString());


                string tableName = null;
                if (element.GetSection("table_name").Value != null)
                    tableName = element.GetSection("table_name").Value.ToString();

                ConfiguredSite cSite = new ConfiguredSite(read_Id, SiteName, cUrl, useDb, youtube);
                
                if (tableName != null)
                    cSite.AddTableName(tableName);

                foreach (var pattern in element.GetSection("allowed_patterns").GetChildren())
                {
                    var pString = pattern.Value.ToString();
                    cSite.AddPattern(pString);
                }

                this.configuredSites.Add(cSite);
            }
        }

        private void Button1_Search_Click(object sender, EventArgs e)
        {
            IConfiguration AppConfig = new ConfigurationBuilder()
                .AddJsonFile("AppConfig.json", optional: false, reloadOnChange: false)
                .Build();

            HistoryPath = AppConfig.GetSection("paths").GetSection("chrome_db").Value;
            Console.WriteLine(HistoryPath);

            var copyDB = AppConfig.GetSection("work_with_db_copy").Value;

            if (copyDB == "False")
            {
                throw new NotImplementedException("Problem with code.");
            }
           
            var fileName = "History"; // this depends on resourcepath?
            if (this.UseExistingDbCopy.Checked == false)
            {
                if (System.IO.File.Exists(ResourcePath + fileName)) // TODO: filename logic not good?
                {
                    System.IO.File.Delete(ResourcePath + fileName);
                }

                try
                {
                    System.IO.File.Copy(HistoryPath, ResourcePath + fileName);
                }
                catch (System.IO.DirectoryNotFoundException)
                {
                    richTextBoxLog.AppendText("There was a problem with the chrome directory. Please configure the correct path for History.\r\n");
                    return;
                }
                richTextBoxLog.AppendText("Copied chrome history for extraction.\r\n");
            }
            else
                richTextBoxLog.AppendText("Using existing coy of ChromeDB.\r\n");
            
            var chromeHistory = new History(new System.IO.FileInfo(ResourcePath + fileName).FullName);
            chromeHistory.ReadData();


            foreach (var site in configuredSites)
            {
                var formattedString = String.Format("Site {0} with pattern {1}: {2} hits\r\n", site.SiteName, site.Url, chromeHistory.CountPatternInUrl(site.Url, site.AllowedPatterns).ToString());
                richTextBoxCounter.AppendText(formattedString);
                //if (histogram.ContainsKey(site.url))
                Console.WriteLine(site.ToString());
            }

            var WriteToDatabaseDialog = MessageBox.Show("Save to DB?", "Database insertion confirmation", MessageBoxButtons.YesNo);
            if (DialogResult.No == WriteToDatabaseDialog)
            {
                return;
            }

            foreach (var site in configuredSites)
            {
                if (site.Use_db == true)
                {
                    if (site.Youtube == true)
                    {
                        var tableName = site.TableName;
                        int numberItemsAdded = chromeHistory.PopulateDatabase(tableName, site.Url, site.AllowedPatterns, browsingConnection, site.Youtube);
                        this.richTextBoxCounter.AppendText(
                        String.Format("Added {0} new items to table: {1}.\r\n",
                            numberItemsAdded.ToString(),
                            tableName
                            )
                        );
                    }
                    else
                    {
                        var tableName = site.TableName;
                        int numberItemsAdded = chromeHistory.PopulateDatabase(tableName, site.Url, site.AllowedPatterns, browsingConnection);
                        this.richTextBoxCounter.AppendText(
                            String.Format("Added {0} new items to table: {1}.\r\n",
                                numberItemsAdded.ToString(),
                                tableName
                                )
                            );
                    }
                }
            }
        }

        private void ConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Configuration configuration = new Configuration();
            configuration.ShowDialog();
        }

        private void SearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var searchYtForm = new SearchYouTubeForm();
            searchYtForm.ShowDialog();
        }

        private void WikipediaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var searchWikiForm = new SearchWikiForm();
            searchWikiForm.ShowDialog();
        }

        private void PDFsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var searchPdfForm = new SearchPdfForm();
            searchPdfForm.ShowDialog();
        }
    }
}
