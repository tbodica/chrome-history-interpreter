﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using WindowsFormsApp1.Models;

namespace WindowsFormsApp1
{
    public partial class SearchYouTubeForm : Form
    {
        private System.Windows.Forms.Timer periodicTimer;
        readonly BackgroundWorker metadataWorker;
        DatabaseSearchController searchController;

        string searchText = "";

        //bool metadataActive = false;
        public SearchYouTubeForm()
        {
            InitializeComponent();

            metadataWorker = new BackgroundWorker();
            metadataWorker.ProgressChanged += MetadataWorker_ProgressChanged;
            metadataWorker.WorkerReportsProgress = true;
        }

        private void MetadataWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void PopulateRichTextBox()
        {
            this.richTextBox1Results.Clear();
            foreach (var result in searchController.results)
            {
                var resString = result.ToString();
                var currentType = result.GetResultType();

                var radioButtonsTypes = new Dictionary<string, RadioButton>
                {
                    { "All", this.radioButton1 },
                    { "video", this.radioButton2 },
                    { "channel", this.radioButton3 },
                    { "search", this.radioButton4 }
                };

                var searchpattern = "";
                if (this.searchText == "")
                    searchpattern = ".*";
                else
                    searchpattern = this.searchText;

                try
                {
                    var regexRes = Regex.Match(resString, searchpattern);
                    var regexChannel = Regex.Match(result.Channel_name, searchpattern);

                    if (!regexRes.Success && !regexChannel.Success)
                    {
                        continue;
                    }
                }
                catch { return; }

                // compare: corresponding radiobutton for result type is SET
                if (!radioButtonsTypes[result.GetResultType()].Checked)
                {
                    continue;
                }

                if (result.GetResultType() == "video")
                {
                    resString += " -> ";
                    resString += result.Channel_name;
                    resString += "\r\n";
                }

                this.richTextBox1Results.AppendText(resString); // point of populating rtb1 with .results
            }
        }

        private void SearchYouTubeForm_Load(object sender, EventArgs e)
        {
            var youtube = true;
            searchController = new DatabaseSearchController("youtube_urls", youtube); // todo config

            PopulateRichTextBox();

            label2TotalCount.Text = searchController.results.Count.ToString();
            label1ProgressVal.Text = this.searchController.Todo_metadata.ToString() + " metadata required";
        }

        private void Button1_Metadata_Click(object sender, EventArgs e)
        {
            if (periodicTimer != null && periodicTimer.Enabled == true)
            {
                periodicTimer.Stop();
                periodicTimer.Dispose();
                return;
            }

            periodicTimer = new System.Windows.Forms.Timer
            {
                Interval = 1200 // 5s
            };
            periodicTimer.Tick += MetadataStep;
            periodicTimer.Start();
        }

        private void MetadataStep(Object source, EventArgs e)
        {
            if (this.searchController.MetadataStep())
            {
                // does entry here imply success of step?
                // if so just decrement
                this.searchController.Todo_metadata -= 1; // todo: handle this by res.Find() in MetadataStep
                this.label1ProgressVal.Text = searchController.Todo_metadata.ToString();
                metadataWorker.ReportProgress((int)((1.0)/((double)(searchController.Todo_metadata)))); // bad logic, but another member can solve
                //this.searchController.Todo_metadata -= 1; // todo: handle this by res.Find() in MetadataStep
                Console.WriteLine("653DEBUG");
                return;
            }
            else
            {
                button1_Metadata.PerformClick();
                label1ProgressVal.Text = "Complete!";
                label1ProgressVal.ForeColor = Color.DarkGreen;
            }

            Console.WriteLine("Step");
            //var progress = this.searchController.results.Count;
            this.label1ProgressVal.Text = this.searchController.Todo_metadata.ToString();
            Console.WriteLine("123DEBUG");
            metadataWorker.ReportProgress(10); // leave this for last
            return;
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButton1.Checked == false)
                return;
            PopulateRichTextBox();
        }

        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButton2.Checked == false)
                return;
            PopulateRichTextBox();
        }

        private void RadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButton3.Checked == false)
                return;
            PopulateRichTextBox();
        }

        private void RadioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (this.radioButton4.Checked == false)
                return;
            PopulateRichTextBox();
        }

        private void TextBox1Search_TextChanged(object sender, EventArgs e)
        {
            this.searchText = textBox1Search.Text;
            PopulateRichTextBox();
        }
    }
}
