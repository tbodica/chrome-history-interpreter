﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Microsoft.Data.Sqlite;

namespace WindowsFormsApp1.Models
{
    public class History
    {
        private readonly string ConnString;
        private readonly string CmdString;
        private List<HistoryUrl> HistoryList { get; }

        public History(string fullHistoryPath)
        {
            ConnString = String.Format(@"Data Source={0}",
                fullHistoryPath
                );

            CmdString = "select url, title, visit_count, datetime(last_visit_time / 1000000 - 11644473600, 'unixepoch', 'localtime') from urls";
            HistoryList = new List<HistoryUrl>();
        }

        public void CreateTable(MySqlConnection connection, string tableName, bool youtube = false)
        {
            if (youtube)
            {
                var createTableCommand = String.Format(
                @"CREATE TABLE `{0}` (
	                `id` INT(11) NOT NULL AUTO_INCREMENT,
	                `browsing_date` DATETIME NOT NULL,
	                `browse_url` VARCHAR(8192) NOT NULL COLLATE 'utf8_unicode_ci',
	                `tags` VARCHAR(4000) NOT NULL COLLATE 'utf8_unicode_ci',
                    `duration` VARCHAR(64) default ('') NOT NULL COLLATE 'utf8_unicode_ci',
                    `channel_name` VARCHAR(2048) default ('') NOT NULL COLLATE 'utf8_unicode_ci',
	                PRIMARY KEY (`id`) USING BTREE
                )
                COLLATE='utf8_unicode_ci'
                ENGINE=InnoDB
                ROW_FORMAT=DYNAMIC
                AUTO_INCREMENT=0
                ;",
    tableName);
                var createCmd = new MySqlCommand(createTableCommand, connection);
                createCmd.ExecuteNonQuery();
            }
            else
            {
                var createTableCommand = String.Format(
                @"CREATE TABLE `{0}` (
	                `id` INT(11) NOT NULL AUTO_INCREMENT,
	                `browsing_date` DATETIME NOT NULL,
	                `browse_url` VARCHAR(8192) NOT NULL COLLATE 'utf8_unicode_ci',
	                `tags` VARCHAR(8192) NOT NULL COLLATE 'utf8_unicode_ci',
	                PRIMARY KEY (`id`) USING BTREE
                )
                COLLATE='utf8_unicode_ci'
                ENGINE=InnoDB
                ROW_FORMAT=DYNAMIC
                AUTO_INCREMENT=0
                ;",
                    tableName);
                var createCmd = new MySqlCommand(createTableCommand, connection);
                createCmd.ExecuteNonQuery();
            }
        }

        public int PopulateDatabase(string tableName, string pattern, List<string> allowedPatterns, MySqlConnection connection, bool youtube = false)
        {
            connection.Open();
            var totalChanged = 0;

            #region create_table_if_doesnt_exist
            var tableExistsCommand = String.Format(
                @"SELECT COUNT(*) 
                    FROM information_schema.tables
                        WHERE table_name = '{0}' 
                    LIMIT 1;",
                tableName
                );

            var searchExistsCmd = new MySqlCommand(tableExistsCommand, connection);
            var searchExistsReader = searchExistsCmd.ExecuteReader();
            if (searchExistsReader.Read())
            {
                var existsResult = searchExistsReader.GetString(0);
                searchExistsReader.Close();
                if (existsResult == "0")
                {
                    CreateTable(connection, tableName, youtube);
                }
                else if (existsResult == "1")
                {

                }
                else 
                {
                    throw new NotImplementedException("SQL query failed.");
                }
            }
            #endregion

            foreach (var item in HistoryList)
            {
                if ( System.Text.RegularExpressions.Regex.IsMatch(item.FullUrl, pattern) && item.MatchesPatterns(allowedPatterns) )
                {
                    var searchExistsCommand = String.Format("SELECT id FROM {0} WHERE browse_url=\"{1}\"", tableName, item.FullUrl);
                    var insertCommand = String.Format("INSERT INTO {0}(browsing_date, browse_url, tags) VALUES (\"{1}\", \"{2}\", \"{3}\")",
                        tableName,
                        item.DateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                        item.FullUrl,
                        item.GetTags()
                        );

                    #region goto_next_if_existing
                    var searchCmd = new MySqlCommand(searchExistsCommand, connection);
                    var searchReader = searchCmd.ExecuteReader();
                    if (searchReader.Read())
                    {
                        searchReader.Close();
                        continue;  // this url already exists in the database, so keep looking in history
                    }
                    searchReader.Close();
                    #endregion

                    var insertCmd = new MySqlCommand(insertCommand, connection);
                    var insertReader = insertCmd.ExecuteReader();
                    insertReader.Read();
                    insertReader.Close();
                    totalChanged += 1;
                    // the current item in the chrome history DB was inserted
                    // next item will be tried
                }
            }

            connection.Close();
            return totalChanged;
        }

        public int CountPatternInUrl(string urlPattern, List<string> urlSubPatternList = null)
        {
            int total = 0;
            foreach (var item in HistoryList)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(item.FullUrl, urlPattern))
                {
                    if (urlSubPatternList != null)
                    {
                        foreach (var urlSubPattern in urlSubPatternList)
                        {
                            if (System.Text.RegularExpressions.Regex.IsMatch(item.FullUrl, urlSubPattern))
                                total += 1;
                        }
                    }
                }
            }

            return total;
        }
        public void ReadData()
        {
            SqliteConnection sqliteConnection = new SqliteConnection(ConnString);
            System.Console.WriteLine(ConnString);
            sqliteConnection.Open();
            SqliteCommand cmd = sqliteConnection.CreateCommand();
            cmd.CommandText = CmdString;
            SqliteDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                System.Console.WriteLine(dr[1].ToString());
                var historyUrl = new HistoryUrl(dr[0].ToString(), dr[1].ToString(), DateTime.Parse(dr[3].ToString()));
                this.HistoryList.Add(historyUrl);
            }

            sqliteConnection.Close();
        }
    }

    public class HistoryUrl
    {
        public string FullUrl { get; }
        public string FullTitle { get; }
        public DateTime DateTime { get; }

        public HistoryUrl(string url, string title, System.DateTime dateTime)
        {
            this.FullUrl = url;
            this.FullTitle = title;
            this.DateTime = dateTime;
        }

        public bool MatchesPatterns(List<string> allowedPatterns)
        {
            foreach (var pattern in allowedPatterns)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(this.FullUrl, pattern))
                    return true;
            }
            return false;
        }


        public string GetTags()
        {
            string rval = "";
            foreach (var item in FullTitle.Split(' '))
            {
                bool okFlag = true;
                foreach (var c in item)
                {
                    if (!(char.IsLetter(c) || char.IsDigit(c) || "-/!&%$#".Contains(c) ))
                    {
                        okFlag = false;
                    }
                }
                if (okFlag)
                {
                    rval += item + ", ";
                }
            }

            return rval.TrimEnd(new char[] {',', ' '} );
        }
    }
}
