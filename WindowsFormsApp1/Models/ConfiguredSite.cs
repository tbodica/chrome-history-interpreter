﻿using MySqlX.XDevAPI.Relational;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Models
{
    class ConfiguredSite
    {
        public Int64 id;
        public string SiteName;
        public string Url { get; }
        public string TableName { get; private set; }
        public List<string> AllowedPatterns { get; }
        public bool Use_db { get; }
        public bool Youtube { get; }

        public ConfiguredSite(Int64 id, string SiteName, string url, bool use_db = false, bool youtube = false)
        {
            this.id = id;
            this.SiteName = SiteName;

            this.Url = url;
            this.Use_db = use_db;
            this.AllowedPatterns = new List<string>();
            this.Youtube = youtube;
        }

        public void AddPattern(string pattern)
        {
            this.AllowedPatterns.Add(pattern);
        }

        public void AddTableName(string tableName)
        {
            this.TableName = tableName;
        }
    }
}
