﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;

using System.Text.Json;

using System.Text.RegularExpressions;
using System.Net.Http;
using System.Net;

namespace WindowsFormsApp1.Models
{
    class DatabaseSearchController
    {
        public List<SearchResult> results;
        readonly MySqlConnection searcherConnection;
        readonly string TableName;

        public int Todo_metadata { get; set; }

        readonly bool youtube;

        public DatabaseSearchController(string tableName, bool youtube = true)
        {
            IConfiguration SqlConfig = new ConfigurationBuilder()
                .AddJsonFile("SqlConfig.json", optional: false, reloadOnChange: false)
                .Build();

            var connectionString = String.Format("server = localhost; port = 3306; userid = {0}; database = browsing; password = {1}; CertificateFile=C:\\Users\\tbodi\\OneDrive\\Documents\\Code\\MariaDB\\SSL_Certs\\ca.pem;SslMode=Required;",
                SqlConfig.GetSection("SearchCredentials").GetSection("username").Value,
                SqlConfig.GetSection("SearchCredentials").GetSection("password").Value
                );

            searcherConnection = new MySqlConnection(connectionString);
            searcherConnection.Open();

            this.results = new List<SearchResult>();
            this.TableName = tableName;
            this.youtube = youtube;

            PopulateResultsList();

        }

        public void PopulateResultsList()
        {
            // using YouTubeUrl
            // using YouTubeUrlHelper
            //var searchQuery = YouTubeUrlHelper.SelectQuery();
            //var l = new List<YouTubeUrl>;
            string searchQuery;
            if (this.youtube)
            {
                searchQuery = String.Format("SELECT id, browse_url, browsing_date, tags, duration, channel_name FROM {0} ORDER BY browsing_date DESC", this.TableName);
            }
            else
            {
                searchQuery = String.Format("SELECT id, browse_url, browsing_date, tags FROM {0} ORDER BY browsing_date DESC", this.TableName);
            }
            

            var searchCmd = new MySqlCommand(searchQuery, this.searcherConnection);
            var searchReader = searchCmd.ExecuteReader();

            while (searchReader.Read())
            {
                var id = searchReader.GetString(0);
                var browse_url = searchReader.GetString(1);
                var date = searchReader.GetString(2);
                var tags = searchReader.GetString(3);

                var duration = "";
                var channel_name = "";

                if (youtube)
                {
                    Console.WriteLine("YOUTUBE!");
                    duration = searchReader.GetString(4);
                    channel_name = searchReader.GetString(5);
                }

                var sr = new SearchResult(id, browse_url, date, tags, duration, channel_name);
                if (channel_name != "")
                    Console.WriteLine(sr.Channel_name);
                this.results.Add(sr);
            }

            searchReader.Close();
            this.Todo_metadata = CountResultsWithoutMetadata();
        }

        public int CountResults()
        {
            int rval = 0;

            var searchQuery = String.Format("SELECT id, browse_url, browsing_date, tags FROM {0}", this.TableName);

            var searchCmd = new MySqlCommand(searchQuery, this.searcherConnection);
            var searchReader = searchCmd.ExecuteReader();

            while (searchReader.Read())
            {
                var res = Regex.Match(searchReader.GetString(1), "(watch\\?v=)[a-zA-Z0-9-_]+[^\\?\\&]");
                if (!res.Success)
                    continue;
                rval += 1;
            }

            searchReader.Close();
            return rval;
        }

        public int CountResultsWithoutMetadata()
        {
            int total = 0;

            foreach (var result in results)
            {
                if (result.GetVideoUrlTermination() == "")
                    continue;
                if (!result.HasMetadata())
                {
                    total += 1;
                }
            }

            return total;
        }

        public bool MetadataStep()
        {
            if (!youtube)
            {
                return false;
            }
            // 1. get first /video/ without metadata [x]
            // 1. obtain metadata from url 
            // 2. change row in DB
            // 3. update item in results List<>
            // 4. ???
            Console.WriteLine("1");
            var searchQuery = "SELECT id, browse_url, browsing_date, tags, duration, channel_name FROM youtube_urls WHERE browse_url LIKE \"%/watch?%\" and channel_name = \"\" ORDER BY browsing_date asc";
            var searchCmd = new MySqlCommand(searchQuery, this.searcherConnection);
            var searchReader = searchCmd.ExecuteReader();

            SearchResult res = null;
            while (searchReader.Read())
            {
                var id = searchReader.GetString(0);
                var browse_url = searchReader.GetString(1);
                var date = searchReader.GetString(2);
                var tags = searchReader.GetString(3);

                var duration = "";
                var channel_name = "";
                Console.WriteLine("point4");
                if (youtube)
                {
                    duration = searchReader.GetString(4);
                    channel_name = searchReader.GetString(5);
                }

                res = new SearchResult(id, browse_url, date, tags, duration, channel_name);
                break;
            }
            searchReader.Close();
            if (res != null)
            {
                var term = res.GetVideoUrlTermination();
                var oembed_url = String.Format("oembed?url=https://www.youtube.com/{0}", term);
                try {
                    using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                    {
                        client.BaseAddress = new Uri("https://www.youtube.com/");
                        HttpResponseMessage response = client.GetAsync(oembed_url).Result;
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Result: " + result);

                        var regex_result = Regex.Match(result, "(\"author_name\")[^,]*");
                        var split = regex_result.Value.ToString().Split('\"');
                        var author_name = split[3].Trim('\"');

                        var regex_result2 = Regex.Match(result, "(\"author_url\")[^,]*");
                        var split2 = regex_result2.Value.ToString().Split('\"');
                        var author_url = split2[3].Trim('\"');

                        Console.WriteLine(author_url);
                        Console.WriteLine(author_name);

                        res.SetChannelName(author_name);

                        var updateQuery = String.Format("UPDATE youtube_urls SET channel_name = '{0}' WHERE id = '{1}'", author_name.Replace('\'', ' ').Replace('\"', ' '), res.GetId().Replace('\'', ' ').Replace('\"', ' '));

                        var updateCmd = new MySqlCommand(updateQuery, this.searcherConnection);
                        var reader = updateCmd.ExecuteNonQuery();

                        // this if() will update local results List<>
                        if (reader > 0)
                        {
                            // bad this.results.Find(res); // too tired
                            int index = 0;
                            bool found = false;
                            foreach (var result_i in results)
                            {

                                if (result_i.IsEqual(res))
                                {
                                    found = true;
                                    break;
                                }
                                index += 1;
                            }

                            if (found)
                            {
                                results[index] = res;
                            }
                            Console.WriteLine("Debugging.....");
                            //this.Todo_metadata = CountResultsWithoutMetadata(); // this would be good, however they must be re-updated from DB beforehand, or other solution found
                            return true;
                        }

                    }
                }
                catch
                {
                    var updateQuery = String.Format("UPDATE youtube_urls SET channel_name = '{0}' WHERE id = '{1}'", "ERROR404".Replace('\'', ' ').Replace('\"', ' '), res.GetId().Replace('\'', ' ').Replace('\"', ' '));
                    var updateCmd = new MySqlCommand(updateQuery, this.searcherConnection);
                    updateCmd.ExecuteNonQuery();
                }

                Console.WriteLine(term);
                Console.WriteLine("point2");
            }
            else
            {
                return false;
            }

            return true;
        }

    }

    class SearchResult
    {
        readonly string id;
        public string Browse_url { get; }
        public string Date { get; }
        public string Tags { get; }

        // metadata members
        public string Duration { get; }
        public string Channel_name { get; set; }
        //string 

        public SearchResult(string id, string browse_url, string date, string tags, string duration, string channel_name)
        {
            this.id = id;
            this.Browse_url = browse_url;
            this.Date = date;
            this.Tags = tags;

            //Console.WriteLine(channel_name);

            this.Duration = duration == "" ? "E00:00" : duration;
            if (channel_name != "")
            {
                Console.WriteLine(channel_name);
            }
            this.Channel_name = (channel_name == "") ? "NO_METADATA" : channel_name;
        }

        public string GetId()
        {
            return this.id;
        }

        public string GetVideoUrlTermination()
        {
            // (watch\?v =)[a-zA - Z0 - 9]+[^\?\&]
            var res = Regex.Match(this.Browse_url, "(watch\\?v=)[a-zA-Z0-9-_]+[^\\?\\&]");
            Console.WriteLine(res.Value.ToString());

            return res.Value.ToString();

        }

        public bool HasMetadata()
        {
            //Console.WriteLine(this.channel_name);
            if ( /*this.channel_name == "E00:00" || */ this.Channel_name == "NO_METADATA")
                return false;
            return true;
        }

        public override string ToString()
        {
            return Date + " -> " + Browse_url.ToString() + "\r\n";
        }

        public bool IsEqual(SearchResult b)
        {
            if (b.Browse_url == this.Browse_url && 
                b.Channel_name == this.Channel_name &&
                b.Date == this.Date &&
                b.Duration == this.Duration &&
                b.GetId() == this.id ) {
                return true;
            }
            return false;
        }

        public void SetChannelName(string author_name)
        {
            this.Channel_name = author_name;
        }

        public string GetResultType()
        {
            var videoRes = Regex.Match(this.Browse_url, "(watch\\?v=)[a-zA-Z0-9-_]+[^\\?\\&]");
            if (videoRes.Success == true)
            {
                return "video";
            }

            var channelRes = Regex.Match(this.Browse_url, "(\\/user\\/|\\/c\\/|\\/channel\\/)");
            if (channelRes.Success)
            {
                return "channel";
            }

            var searchRes = Regex.Match(this.Browse_url, "\\/results\\?");
            if (searchRes.Success)
            {
                return "search";
            }

            return "All";
        }
    }
}
