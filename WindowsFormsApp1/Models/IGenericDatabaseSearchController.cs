﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

using MySql.Data.MySqlClient;

using Microsoft.Extensions.Configuration;


namespace WindowsFormsApp1.Models
{
    class IGenericDatabaseSearchController
    {
        public List<IGenericSearchResult> results;
        readonly MySqlConnection searcherConnection;
        readonly string TableName;
        
        public IGenericDatabaseSearchController(string tableName)
        {
            IConfiguration SqlConfig = new ConfigurationBuilder()
                .AddJsonFile("SqlConfig.json", optional: false, reloadOnChange: false)
                .Build();

            var connectionString = String.Format("server = localhost; port = 3306; userid = {0}; database = browsing; password = {1}; CertificateFile=C:\\Users\\tbodi\\OneDrive\\Documents\\Code\\MariaDB\\SSL_Certs\\ca.pem;SslMode=Required;",
                SqlConfig.GetSection("SearchCredentials").GetSection("username").Value,
                SqlConfig.GetSection("SearchCredentials").GetSection("password").Value
                );

            this.searcherConnection = new MySqlConnection(connectionString);
            this.searcherConnection.Open();

            this.results = new List<IGenericSearchResult>();
            this.TableName = tableName;
            PopulateResultsList();
        }

        public void PopulateResultsList()
        {
            string searchQuery;

            searchQuery = String.Format("SELECT id, browse_url, browsing_date, tags FROM {0} ORDER BY browsing_date DESC", this.TableName);



            var searchCmd = new MySqlCommand(searchQuery, this.searcherConnection);
            var searchReader = searchCmd.ExecuteReader();

            while (searchReader.Read())
            {
                var id = searchReader.GetString(0);
                var browse_url = searchReader.GetString(1);
                var date = searchReader.GetString(2);
                var tags = searchReader.GetString(3);

                var sr = new IGenericSearchResult(id, browse_url, date, tags);

                this.results.Add(sr);
            }

            searchReader.Close();
        }
    }

    class IGenericSearchResult
    {
        readonly string id;
        public string Browse_url { get; }
        public string Date { get; }
        public string Tags { get; }


        public IGenericSearchResult(string id, string browse_url, string date, string tags)
        {
            this.id = id;
            this.Browse_url = browse_url;
            this.Date = date;
            this.Tags = tags;
        }

        public string GetId()
        {
            return this.id;
        }


        public override string ToString()
        {
            return Date + " -> " + Browse_url.ToString() + "\r\n";
        }


        public string GetResultType()
        {
            /*
             * todo diff pattrn
             */


            var searchRes = Regex.Match(this.Browse_url, "\\&search="); // unsure
            if (searchRes.Success)
            {
                return "search";
            }

            return "All";
        }
    }
}
