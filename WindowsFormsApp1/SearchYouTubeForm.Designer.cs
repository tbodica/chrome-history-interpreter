﻿namespace WindowsFormsApp1
{
    partial class SearchYouTubeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1Search = new System.Windows.Forms.TextBox();
            this.richTextBox1Results = new System.Windows.Forms.RichTextBox();
            this.label1Counter = new System.Windows.Forms.Label();
            this.label2TotalCount = new System.Windows.Forms.Label();
            this.button1_Metadata = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1ProgressDesc = new System.Windows.Forms.Label();
            this.label1ProgressVal = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // textBox1Search
            // 
            this.textBox1Search.Location = new System.Drawing.Point(12, 32);
            this.textBox1Search.Name = "textBox1Search";
            this.textBox1Search.Size = new System.Drawing.Size(344, 20);
            this.textBox1Search.TabIndex = 0;
            this.textBox1Search.TextChanged += new System.EventHandler(this.TextBox1Search_TextChanged);
            // 
            // richTextBox1Results
            // 
            this.richTextBox1Results.Location = new System.Drawing.Point(12, 137);
            this.richTextBox1Results.Name = "richTextBox1Results";
            this.richTextBox1Results.Size = new System.Drawing.Size(568, 301);
            this.richTextBox1Results.TabIndex = 1;
            this.richTextBox1Results.Text = "";
            // 
            // label1Counter
            // 
            this.label1Counter.AutoSize = true;
            this.label1Counter.Location = new System.Drawing.Point(12, 110);
            this.label1Counter.Name = "label1Counter";
            this.label1Counter.Size = new System.Drawing.Size(41, 13);
            this.label1Counter.TabIndex = 2;
            this.label1Counter.Text = "Count: ";
            // 
            // label2TotalCount
            // 
            this.label2TotalCount.AutoSize = true;
            this.label2TotalCount.Location = new System.Drawing.Point(60, 109);
            this.label2TotalCount.Name = "label2TotalCount";
            this.label2TotalCount.Size = new System.Drawing.Size(0, 13);
            this.label2TotalCount.TabIndex = 3;
            // 
            // button1_Metadata
            // 
            this.button1_Metadata.Location = new System.Drawing.Point(713, 325);
            this.button1_Metadata.Name = "button1_Metadata";
            this.button1_Metadata.Size = new System.Drawing.Size(152, 36);
            this.button1_Metadata.TabIndex = 4;
            this.button1_Metadata.Text = "Populate Metadata";
            this.button1_Metadata.UseVisualStyleBackColor = true;
            this.button1_Metadata.Click += new System.EventHandler(this.Button1_Metadata_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(713, 367);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(152, 23);
            this.progressBar1.TabIndex = 5;
            // 
            // label1ProgressDesc
            // 
            this.label1ProgressDesc.AutoSize = true;
            this.label1ProgressDesc.Location = new System.Drawing.Point(713, 397);
            this.label1ProgressDesc.Name = "label1ProgressDesc";
            this.label1ProgressDesc.Size = new System.Drawing.Size(51, 13);
            this.label1ProgressDesc.TabIndex = 6;
            this.label1ProgressDesc.Text = "Progress:";
            // 
            // label1ProgressVal
            // 
            this.label1ProgressVal.AutoSize = true;
            this.label1ProgressVal.Location = new System.Drawing.Point(755, 396);
            this.label1ProgressVal.Name = "label1ProgressVal";
            this.label1ProgressVal.Size = new System.Drawing.Size(0, 13);
            this.label1ProgressVal.TabIndex = 7;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(601, 150);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(74, 17);
            this.radioButton1.TabIndex = 8;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "All Results";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(601, 173);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(57, 17);
            this.radioButton2.TabIndex = 9;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Videos";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.RadioButton2_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(601, 196);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(69, 17);
            this.radioButton3.TabIndex = 10;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Channels";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.RadioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(601, 219);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(70, 17);
            this.radioButton4.TabIndex = 11;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Searches";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.RadioButton4_CheckedChanged);
            // 
            // SearchYouTubeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 450);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label1ProgressVal);
            this.Controls.Add(this.label1ProgressDesc);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button1_Metadata);
            this.Controls.Add(this.label2TotalCount);
            this.Controls.Add(this.label1Counter);
            this.Controls.Add(this.richTextBox1Results);
            this.Controls.Add(this.textBox1Search);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(893, 489);
            this.MinimumSize = new System.Drawing.Size(893, 489);
            this.Name = "SearchYouTubeForm";
            this.Text = "SearchYouTubeForm";
            this.Load += new System.EventHandler(this.SearchYouTubeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1Search;
        private System.Windows.Forms.RichTextBox richTextBox1Results;
        private System.Windows.Forms.Label label1Counter;
        private System.Windows.Forms.Label label2TotalCount;
        private System.Windows.Forms.Button button1_Metadata;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1ProgressDesc;
        private System.Windows.Forms.Label label1ProgressVal;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
    }
}