﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microsoft.Extensions.Configuration;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace WindowsFormsApp1
{
    public partial class Configuration : Form
    {
        public Configuration()
        {
            InitializeComponent();
        }

        private void Button1BrowseChrome_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            fd.ShowDialog();
            var result = fd.FileName.ToString();
            textBox1_chromePath.Text = result;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            var jsonString = System.IO.File.ReadAllText("AppConfig.json");
            AppConfigJson cfg = JsonSerializer.Deserialize<AppConfigJson>(jsonString);
            cfg.Paths.Chrome_db = textBox1_chromePath.Text;
            //cfg.toggleSettings.WorkWithDbCopy = checkBox1_CopyDB.Checked.ToString();
            cfg.Work_with_db_copy = checkBox1_CopyDB.Checked.ToString();
            Console.WriteLine(checkBox1_CopyDB.Checked.ToString());
            var finalJsonString = JsonSerializer.Serialize(cfg);
            System.IO.File.WriteAllText("AppConfig.json", finalJsonString);
        }

        private void Configuration_Load(object sender, EventArgs e)
        {
            IConfiguration AppConfig = new ConfigurationBuilder()
                .AddJsonFile("AppConfig.json", optional: false, reloadOnChange: false)
                .Build();
            textBox1_chromePath.Text = AppConfig.GetSection("paths").GetSection("chrome_db").Value;
            checkBox1_CopyDB.Checked = (bool)(AppConfig.GetSection("work_with_db_copy").Value == "True");
        }
    }

    public class AppConfigJson
    {
        public AppConfigPath Paths { get; set; }
        public string Work_with_db_copy { get; set; }
    }

    public class AppConfigPath
    {
        public string Chrome_db { get; set; }
    }
}
