import matplotlib.pyplot as plt
import datetime

fd = open("b.txt", 'r')
data = fd.read()

datedict = {}
tupvec = []

processed_count = 0
for i in data.split('\n'):

    vals = i.split(',,,,')


    dates = vals[0]
    try:
        urls = vals[2]
    except:
        continue

    day = dates.split(' ')[0]

    # C++
    if day in datedict.keys():
        datedict[day].append((dates, urls))
    else:
        datedict[day] = []
        datedict[day].append((dates, urls))

    processed_count += 1


date_objects = []

print(processed_count)
# need day with freq
for i in datedict.keys():
    print(i + ' ' + str(len(datedict[i])))
    date_str = i
    date_ctr = len(datedict[i])

    date_objects.append(
        (datetime.datetime.strptime(date_str, '%Y-%m-%d').date(), date_ctr)
    )



#to add: histogram domains

dates_srt = [d[0] for d in date_objects]
vals_srt = [d[1] for d in date_objects]
print(dates_srt)
print(vals_srt)
plt.plot(dates_srt, vals_srt)
plt.show()