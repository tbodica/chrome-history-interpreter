
import sys
import mariadb

fd = open('b.txt', 'r')
data = fd.read()
fd.close()

ncbi_count = 0

push_database = False


conn = None
cursor = None
if (len(sys.argv) > 1):
    if (sys.argv[1] == "insertdb"):
        push_database = True
        try:
            conn = mariadb.connect(
                user="[redacted]",
                password="[redacted]",
                host="localhost",
                port=3306)
            cursor = conn.cursor()
            cursor.execute("USE browsing_data")
        except mariadb.Error as e:
                print(f"Error connecting to MariaDB Platform: {e}")
                sys.exit(1)

for i in data.split('\n'):
    
    vals = i.split(',,,,')


    dates = vals[0]
    title = ""
    try:
        title = vals[2]
    except:
        continue

    split_words = title.split('/')[-1].split('-')
    all_words = {} # will contain common word freq
    for word in split_words:
        flag_append = True
        for lt in word:
            if not lt.isalpha() and not lt.isdecimal():
                flag_append = False
                break
        if flag_append == True:
            if word not in all_words.keys():
                all_words[word] = 1
            else:
                all_words[word] = all_words[word] + 1

    if "stackoverflow.com/questions" in title:
        ncbi_count += 1
        print(title + ' -> ' + dates)

        wordfreq_string = ""
        for i in all_words.keys():
            wordfreq_string += "{0}:{1} ".format(str(i), str(all_words[i]))
        wordfreq_string.rstrip(" ")
        

        if (push_database == True):
            queryString = "INSERT INTO stackoverflow_questions SET "
            queryString += "date=\"{0}\", ".format(dates)
            queryString += "url=\"{0}\", ".format(title)
            queryString += "wordfreq_str=\"{0}\" ".format(wordfreq_string)

            #print("querystring is: {0}".format(queryString)
            cursor.execute(queryString)


print(push_database)
print(ncbi_count)