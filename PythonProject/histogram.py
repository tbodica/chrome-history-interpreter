
import matplotlib.pyplot as plt

fd = open('b.txt', 'r')
data = fd.read()

uniques_dict = {}

for i in data.split('\n'):
    
    vals = i.split(',,,,')


    dates = vals[0]
    title = ""
    try:
        title = vals[2].split('.')[1].split('/')[0]
    except:
        continue

    if title not in uniques_dict:
        uniques_dict[title] = 1
    else:
        uniques_dict[title] = uniques_dict[title] + 1

#     if "ncbi" in title:
#         ncbi_count += 1
#         print(title)

# print(ncbi_count)

as_tup = []

for i in uniques_dict.keys():
    as_tup.append((uniques_dict[i], i))

as_tup.sort()
print(as_tup)

as_tup = as_tup[::-1]
as_tup = as_tup[:20]

tup_x = []
tup_y = []

for i in as_tup:
    tup_x.append(i[0])
    tup_y.append(i[1])

plt.bar(tup_y, tup_x)
plt.show()