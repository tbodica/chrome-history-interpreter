# chrome-history-interpreter

Basic script infrastructure for gaining access to Chrome's history, and extracting data for statistical inquiries and for self-studies.

Instructions:

The script `code` expects a History sql3 file to exist in path, will output as .csv
The script `count_papers` will count mentions of 'ncbi' in the domain part of every entry, from the .csv
The script `plot` will plot a spread of activity, number of history entries per date
The script `histogram` will plot a histogram of unique domains by activity

Meant to be taken as a 30 minute or 1 hour activity for satisfying a quick curiosity.

![plot](Images/sample_plot.PNG)
![winforms](Images/WinForms.PNG)